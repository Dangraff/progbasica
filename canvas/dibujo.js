//Graffitianda en JS

var d = document.getElementById( 'dibujito' );
var lienzo = /*ID mas el get*/ d.getContext( '2d' );
var lineas = 30;
var l = 0;
var c = 0;
var yi, xf;
var xi, yf;


while( l < lineas )
{
    yi = 10 * l;
    xf = 10 * ( l + 1 );
    dibujarLinea( 'red', 0, yi, xf, 300);

    l = l + 1;

}

dibujarLinea( 'red', 0, 300, 300, 300 );
dibujarLinea( 'red', 0, 0, 0, 300 );


while( c < lineas )
{
    xi = 10 * c;
    yf = 10 * ( c + 1 );
    dibujarLinea( 'blue', xi, 0, 300, yf);

    c = c + 1;

}

dibujarLinea( 'blue', 300, 300, 300, 0 );
dibujarLinea( 'blue', 300, 0, 0, 0 );

function dibujarLinea(color, xInicial, yInicial, xFinal, yFinal )
{
    lienzo.beginPath();
    lienzo.strokeStyle = color;
    lienzo.moveTo( xInicial, yInicial );
    lienzo.lineTo( xFinal, yFinal );
    lienzo.stroke();
    lienzo.closePath();
}